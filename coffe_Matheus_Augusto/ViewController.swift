//
//  ViewController.swift
//  coffe_Matheus_Augusto
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Cafe: Decodable {
    let file : String
}

class ViewController: UIViewController {
    
    var listaDeCafe: [Cafe]?
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBAction func recarregarImagem(_ sender: Any) {
        GetCafe()
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetCafe()
    }
    
    func GetCafe(){
        AF.request("https://coffee.alexflipnote.dev/random.json")
            .responseDecodable(of: Cafe.self){
                response in
                if let cafe = response.value{
                    self.imageView.kf.setImage(with: URL(string: cafe.file))
                    print(cafe.file)
                }
            }
    }


}

//constraints:

//Imagem - 37 esquerda, 37 direita, 60 top space

//Botao - 37 esquerda, 37 direita, 20 top space



